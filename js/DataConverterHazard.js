import {UtilApplications} from "./UtilApplications.js";
import {UtilDataConverter} from "./UtilDataConverter.js";
import {DataConverterJournal} from "./DataConverterJournal.js";

class DataConverterHazard extends DataConverterJournal {
	static get _CONFIG_GROUP () { return "importHazard"; }

	/**
	 * @param haz
	 * @param [opts] Options object.
	 * @param [opts.isAddOwnership]
	 * @param [opts.defaultOwnership]
	 */
	static async pGetHazardJournal (haz, opts) {
		opts = opts || {};

		const content = await UtilDataConverter.pGetWithDescriptionPlugins(() => {
			const subtitle = Renderer.traphazard.getSubtitle(haz);
			return `<div>
				${subtitle ? `<div class="mb-1 italic">${subtitle}</div>` : ""}
				${Renderer.get().setFirstSection(true).render({entries: haz.entries}, 2)}
			</div>`;
		});

		const img = await this._pGetSaveImagePath(haz);

		const name = UtilApplications.getCleanEntityName(UtilDataConverter.getNameWithSourcePart(haz));
		const out = {
			name,
			pages: this._getPages({name, content, img}),
			ownership: {default: 0},
		};

		out.pages.forEach(page => this._mutOwnership(page, opts));
		this._mutOwnership(out, opts);

		return out;
	}
}

export {DataConverterHazard};
