import {UtilApplications} from "./UtilApplications.js";
import {UtilDataConverter} from "./UtilDataConverter.js";
import {DataConverterJournal} from "./DataConverterJournal.js";

class DataConverterVariantRule extends DataConverterJournal {
	static get _CONFIG_GROUP () { return "importRule"; }

	/**
	 * @param rule
	 * @param [opts] Options object.
	 * @param [opts.isAddOwnership]
	 * @param [opts.defaultOwnership]
	 */
	static async pGetVariantRuleJournal (rule, opts) {
		opts = opts || {};

		const cpy = MiscUtil.copy(rule);
		delete cpy.name;
		delete cpy.page;
		delete cpy.source;

		const content = await UtilDataConverter.pGetWithDescriptionPlugins(() => `<div>${Renderer.get().setFirstSection(true).render(cpy)}</div>`);

		const img = await this._pGetSaveImagePath(rule);

		const name = UtilApplications.getCleanEntityName(UtilDataConverter.getNameWithSourcePart(rule));
		const out = {
			name,
			pages: this._getPages({name, content, img}),
			ownership: {default: 0},
		};

		out.pages.forEach(page => this._mutOwnership(page, opts));
		this._mutOwnership(out, opts);

		return out;
	}
}

export {DataConverterVariantRule};
