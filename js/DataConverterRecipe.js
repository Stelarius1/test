import {UtilApplications} from "./UtilApplications.js";
import {UtilDataConverter} from "./UtilDataConverter.js";
import {DataConverterJournal} from "./DataConverterJournal.js";

class DataConverterRecipe extends DataConverterJournal {
	static get _CONFIG_GROUP () { return "importRecipe"; }

	/**
	 * @param recipe
	 * @param [opts] Options object.
	 * @param [opts.isAddOwnership]
	 * @param [opts.defaultOwnership]
	 */
	static async pGetRecipeJournal (recipe, opts) {
		opts = opts || {};

		const content = await UtilDataConverter.pGetWithDescriptionPlugins(() => Renderer.recipe.getBodyHtml(recipe));

		const fluff = await Renderer.utils.pGetFluff({
			entity: recipe,
			fluffUrl: `data/fluff-recipes.json`,
			fluffProp: "recipeFluff",
		});

		const img = await this._pGetSaveImagePath(recipe, {fluff});

		const name = UtilApplications.getCleanEntityName(UtilDataConverter.getNameWithSourcePart(recipe));
		const out = {
			name,
			pages: this._getPages({name, content, img}),
			ownership: {default: 0},
		};

		out.pages.forEach(page => this._mutOwnership(page, opts));
		this._mutOwnership(out, opts);

		return out;
	}
}

export {DataConverterRecipe};
