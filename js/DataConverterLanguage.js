import {UtilApplications} from "./UtilApplications.js";
import {UtilDataConverter} from "./UtilDataConverter.js";
import {DataConverterJournal} from "./DataConverterJournal.js";

class DataConverterLanguage extends DataConverterJournal {
	static get _CONFIG_GROUP () { return "importLanguage"; }

	/**
	 * @param ent
	 * @param [opts] Options object.
	 * @param [opts.isAddOwnership]
	 * @param [opts.defaultOwnership]
	 */
	static async pGetLanguageJournal (ent, opts) {
		opts = opts || {};

		const content = await UtilDataConverter.pGetWithDescriptionPlugins(() => `<div>${Renderer.language.getRenderedString(ent, {isSkipNameRow: true})}</div>`);

		const fluff = await Renderer.utils.pGetFluff({
			entity: ent,
			fluffUrl: `data/fluff-languages.json`,
			fluffProp: "languageFluff",
		});

		const img = await this._pGetSaveImagePath(ent, {fluff});

		const name = UtilApplications.getCleanEntityName(UtilDataConverter.getNameWithSourcePart(ent));
		const out = {
			name,
			pages: this._getPages({name, content, img}),
			ownership: {default: 0},
		};

		out.pages.forEach(page => this._mutOwnership(page, opts));
		this._mutOwnership(out, opts);

		return out;
	}
}

export {DataConverterLanguage};
